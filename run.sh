#!/bin/bash
##================CHECK OS VERSION===============
if ! [ $(id -u) = 0 ];
then
	echo "This script must be run as root" 1>&2
   	exit 1
else
	echo "=================CHECK YOUR OS VERSION============="
	OS=$(cat /etc/*release | grep PRETTY_NAME | sed 's:.*=::' | tr -d '",' | awk '{print $1}')
	if [ "$OS" = "Red" ] || [ "$OS" = "CentOS" ] || [ "$OS" = "Amazon" ];
	then
        	cat /etc/*release | grep PRETTY_NAME | sed 's:.*=::'
			echo "==================================================="
	        echo "=====================Add Repo======================"
        	epel=$(yum info epel-release | grep Repo | awk '{ print $3 }')
		remi=$(yum info remi-release | grep Repo | awk 'NR==1{ print $3 }')
        	if [ "$repo" != "installed" ] && [ "$remi" != "installed" ];
        	then
	                yum -y install epel-release
	                yum install –y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        	        yum install –y https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
			yum -y install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
			yum -y install http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
               		yum-config-manager --enable epel
			yum-config-manager --enable remi
                	echo "==================================================="
        	else
                	echo "Repo epel,remi installed !!!!"
			echo "==================================================="
	        fi
        	echo -e 'What webserver do you want to install? \n 1)Apache \n 2)Nginx'
	        read -p 'Enter your choise: ' webserver
	        while [ $webserver != 1 ] && [ $webserver != 2 ];do
                	echo -e 'Please choise \n 1)Apache \n 2)Nginx'
	                read -p 'Enter your choise: ' webserver
		done	
        	if [ $webserver == '1' ];then
                	httpd=$(yum info httpd | grep Repo | awk '{ print $3 }')
	                if [ "$httpd" != "installed" ];
        	        then
                	        read -p "Apache not intsall . Enter to install....."
                        	yum install -y httpd
	                        service httpd restart
        	                echo "Config file httpd.conf"
                	        sed -i '151s/AllowOverride None/AllowOverride All/g' /etc/httpd/conf/httpd.conf
                        	echo -e '# add follows to the \n # servers response header \n ServerTokens Prod \n # keepalive is ON \n KeepAlive On' >> /etc/httpd/conf/httpd.conf
	                else
        	                echo "Apache installed !!!"
				echo "==================================================="
                	fi
		elif [ $webserver == '2' ];then
			nginx=$(yum info nginx | grep Repo | awk '{ print $3 }')
                	if [ "$nginx" != "installed" ];
	                then
        	                read -p "Nginx not intsall . Enter to install....."
	                	yum --enablerepo=epel -y install nginx
				service nginx restart
			else
	                       	echo "Nginx installed !!!"
				echo "==================================================="
        	        fi
		fi
		php=$(yum info php | grep Repo | awk '{ print $3 }')
                        if [ "$php" != "installed" ];
                        then
                                read -p "php not intsall . Enter to install....."
                                yum --enablerepo=epel -y install php php-mbstring php-pear php-fpm
                                service nginx restart
				service httpd restart
				echo ""
                        else
                                echo "PHP installed !!!"
				echo "==================================================="
                        fi
		mysql=$(yum info mysql* | grep Repo | awk 'NR==1{ print $3 }')
                        if [ "$mysql" != "installed" ];
                        then
				read -p "Mysql not intsall . Enter to install....."
                                        yum install -y mysql-server
					service mysqld restart
                                        mysql_secure_installation
			else
                                echo "Mysql installed !!!"
                        fi
        elif [ "$OS" = 'Ubuntu' ];
	then
        	os=$(cat /etc/*release | grep PRETTY_NAME | sed 's:.*=::')
		echo $os
		dpkg -l php > /dev/null 2>&1
		php=$?
        	if [ $php == '0' ]; then
                	echo "PHP was installed!!!!!"
			echo"==================================================="
        	else
			read -p "PHP not install, Enter to install........."                
			 apt-get -y install php php-fpm
        	fi
                
		echo -e 'what webserver do you want install? \n 1)Apache \n 2)Nginx '
                read -p 'Enter your choise: ' webserver
                #while : [ $webserver != 1 ] && [ $webserver != 2 ]; do
               	while [[ -z $websierver && $webserver != 1 && $webserver != 2 ]]; do
			echo -e 'Please choise \n 1)Apache \n 2)Nginx'
                        read -p 'Enter your choise: ' webserver
        	done
			if [ $webserver == '1' ];
			then
				dpkg -l  apache2 > /dev/null 2>&1
        			apache2=$?
			        if [ $apache2 == '0' ]; then
		        		echo "Apache was installed, Enter to continous"
					echo "==================================================="
			        else
			             	read -p "Apache not install, Enter to install ......."
					sudo apt-get install apache2;
					sed -i ''\s'/OS/Prod/'\g''  /etc/apache2/conf-enabled/security.conf
					/etc/init.d/apache2 restart
       				fi
			elif [ $webserver == '2' ];
			then
				dpkg -l nginx > /dev/null 2>&1
       				nginx=$?
			        if [ $nginx == '0' ]; then
		                	echo "Nginx was installed!!!!"
					echo "==================================================="
		        	else
			                read -p "Nginx not install, Enter to install ......."
					sudo apt-get install -y nginx
					/etc/init.d/nginx restart
			        fi
			fi
		dpkg -l mysql-server > /dev/null 2>&1
       		mysql=$?
		if [ $mysql == '0' ]; then
                	echo "Mysql was installed!!!!!"
			echo "==================================================="
        	else
        		read -p "Mysql not install, Enter to install......."
			apt-get -y install mysql-server
		fi
	fi
fi
echo "==================================================="
echo "FINISHED TO CHECK WEBSERVER, DBSERVER, PHP"
echo "==================================================="